from urllib.request import urlopen
from typing import Union
import io

import requests

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from dateutil.parser import parse
from pytz import timezone
import pandas as pd
from numpy import float64, int32, ndarray
import xml.etree.ElementTree as ET

from geojson import Point, Feature, FeatureCollection


def parse_handling_hours(x, y):
    if isinstance(x, ndarray):
        return [parse(d[0]) + relativedelta(hours=int(d[1])) for d in zip(x, y)]

    return parse(x) + relativedelta(hours=int(y))


def read_knmi_header(fid):
    started = False
    station_data = {}
    for line in fid:
        if started and len(line) == 3:
            break
        if started:
            station_data[int(line[2:line.find(":")])] = [float(line[10:24]),
                                                         float(line[24:38]),
                                                         float(line[38:46]),
                                                         line[46:-1]]
        if line.startswith("# STN"):
            started = True
    d = pd.DataFrame.from_dict(station_data, orient='index')
    d.rename(columns={0: "LAT", 1: "LON", 2: "ALT", 3: "NAME"}, inplace=True)
    return d


def knmi_sml_locations(source: Union[pd.DataFrame]) -> dict:
    e = {}
    for station in source.iterrows():
        e[station[0]] = ET.fromstring("\
<sml:position xmlns:sml=\"http://www.opengis.net/sensorml/2.0\"\
              xmlns:gml=\"http://www.opengis.net/gml/3.2\">\
      <gml:Point gml:id=\"stationLocation\" srsName=\"http://www.opengis.net/def/crs/EPSG/0/4326\">\
         <gml:coordinates>{} {}</gml:coordinates>\
      </gml:Point>\
   </sml:position>".format(station[1]['LAT'], station[1]['LON']))
    return e


def knmi_geojson(source: Union[pd.DataFrame]) -> FeatureCollection:
    features = []
    for station in source.iterrows():
        features.append(Feature(geometry=Point((station[1]['LAT'], station[1]['LON'])),
                                properties={"Name": station[1]['NAME']}))
    return FeatureCollection(features)

def read_knmi_data(fid):
    station_list = False
    column_names = ""
    for line in fid:
        print(line)
        if line.startswith("# STN"):
            if station_list:
                column_names = line.split(",")
            else:
                station_list = True
    column_names = [column_name.strip(" #\r\n") for column_name in column_names]
    dtypes = dict(zip(column_names, [float64 for column_name in column_names]))
    dtypes['STN'] = int32
    if "HH" in column_names:
        dtypes['YYYYMMDD'] = str
        dtypes['HH'] = int32
        datecolumns = [[1, 2]]
        parser = parse_handling_hours
    else:
        dtypes['YYYYMMDD'] = str
        datecolumns = [1]
        parser = None
    fid.seek(0)
    d = pd.read_csv(fid,
                    comment='#',
                    header=0,
                    names=column_names,
                    parse_dates=datecolumns,
                    date_parser=parser,
                    dtype=dtypes,
                    skipinitialspace=True).fillna(-1)
    return d


def read_knmi_hourly_data(fid):
    d = read_knmi_data(fid)
    d.set_index('YYYYMMDD_HH', inplace=True)
    return d


def read_knmi_daily_data(fid):
    d = read_knmi_data(fid)
    d.set_index('YYYYMMDD', inplace=True)

    d['FHVEC'] /= 10.
    d['FG'] /= 10.
    d['FHX'] /= 10.
    d['FHN'] /= 10.
    d['FXX'] /= 10.
    d['TG'] /= 10.
    d['TN'] /= 10.
    d['TX'] /= 10.
    d['T10N'] /= 10.
    d['SQ'] /= 10.
    d['DR'] *= 10.
    d['RH'] /= 10.
    d['RHX'] /= 10.
    d['EV24'] / 10.
    d['VVN'] *= 100.

    d['VVX'] *= 100.
    return d


def get_knmi_hourly_data(start, end):
    url = 'http://projects.knmi.nl/klimatologie/uurgegevens/getdata_uur.cgi'
    data = b'start=20170901&stns=270&vars=WIND:TEMP:SUNR:PRCP:VICL'
    with urlopen(url, data=data) as fid:
        d = read_knmi_hourly_data(fid)
    return d


def get_knmi_data(start: datetime = None, end: datetime = None, stns: list =
                  None, hourly: bool = False, url: str = None)\
        -> pd.DataFrame:
    """
    Download weather data from the KNMI server and return the data as a DataFrame.

    Arguments
    ---------
    start: datetime
        Start date of the data, if not specified or None set to 12 weeks before end
    end: datetime
        End date of the data, if not specified or None set to now()
    stns: list
        List of station numbers to download the data from
    hourly: bool
        If set to True fetches the hourly data, otherwise the daily aggregates are downloaded
    url: str
        Allows to override the base url

    Returns
    -------
    d: DataFrame
        The requested data
    """
    if url is None:
        if hourly:
            url = "http://projects.knmi.nl/klimatologie/uurgegevens/getdata_uur.cgi"
        else:
            url = 'http://projects.knmi.nl/klimatologie/daggegevens/getdata_dag.cgi'

    payload = {}
 
    if end is None:
        end = datetime.now()
    if start is None:
        start = end - timedelta(weeks=12)

    if hourly:
        payload['end'] = end.strftime("%Y%m%d24")
        payload['start'] = start.strftime("%Y%m%d01")
    else:
        payload['end'] = end.strftime("%Y%m%d")
        payload['start'] = start.strftime("%Y%m%d")

    if stns:
        payload['stns'] = stns
    else:
        payload['stns'] = 270

    payload['vars'] = "ALL"  # ""WIND:TEMP:SUNR:PRCP:VICL"

    r = requests.get(url, payload)
    with io.StringIO(r.text) as fid:
        if hourly:
            d = read_knmi_hourly_data(fid)
        else:
            d = read_knmi_daily_data(fid)
    d.index = d.index.tz_localize(timezone('UTC'))
    return d