from scipy.signal import savgol_filter
import pandas as pd
import numpy as np

def smooth_steplike(data, method='polynomial', order=2):

    smoothdata = data

    # replace step-like function to interpolated one
    firstvalues = np.ones(len(data))
    firstvalues[1:] = data[1:].values - data[:-1].values
    smoothdata[firstvalues==0] = np.nan
    smoothdata = smoothdata.interpolate(method=method, order=order)

    return smoothdata