import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
import pandas as pd
import matplotlib.dates as mdates

def calcSpectrogram(sig, fs=1, NFFT=256):
    # calcuate the spectrogram from sig timeseries
    # with sampling rate fs

    spec, freq, t = mlab.specgram(sig, Fs=fs, NFFT=NFFT)

    # calculate the bin limits in time (x dir)
    # note that there are n+1 fence posts
    dt = t[1] - t[0]
    t_edge = np.empty(len(t) + 1)
    t_edge[:-1] = t - dt / 2.
    # however, due to the way the spectrogram is calculates, the first and last bins
    # a bit different:
    t_edge[0] = 0
    t_edge[-1] = t_edge[0] + len(sig) / fs

    # calculate the frequency bin limits:
    df = freq[1] - freq[0]
    freq_edge = np.empty(len(freq) + 1)
    freq_edge[:-1] = freq - df / 2.
    freq_edge[-1] = freq_edge[-2] + df

    return t_edge, freq_edge, spec, dt


def showSpectrogram(sig, fs=1, NFFT=256, time_format = ''):
    # calcuate the spectrogram from sig timeseries
    # with sampling rate fs

    spec, freq, t = mlab.specgram(sig, Fs=fs, NFFT=NFFT)

    # calculate the bin limits in time (x dir)
    # note that there are n+1 fence posts
    dt = t[1] - t[0]
    t_edge = np.empty(len(t) + 1)
    t_edge[:-1] = t - dt / 2.
    # however, due to the way the spectrogram is calculates, the first and last bins
    # a bit different:
    t_edge[0] = 0
    t_edge[-1] = t_edge[0] + len(sig) / fs

    # calculate the frequency bin limits:
    df = freq[1] - freq[0]
    freq_edge = np.empty(len(freq) + 1)
    freq_edge[:-1] = freq - df / 2.
    freq_edge[-1] = freq_edge[-2] + df

    # calculate the period bin limits, omit the zero frequency bin
    p_edge = 1. / freq_edge[1:]

    # we'll plot both
    fig = plt.figure(figsize=(12,8))
    ax1 = fig.add_subplot(211)
    if time_format == '':
        ax1.pcolormesh(t_edge, freq_edge, np.log10(spec))
    else:
        t_axis = sig.index.astype('int64')
        t_axis_plot = np.linspace(t_axis.min(), t_axis.max(), len(t))
        t_axis_plot = pd.to_datetime(t_axis_plot)
        ax1.pcolormesh(t_axis_plot, freq_edge, np.log10(spec))

        myFmt = mdates.DateFormatter(time_format)
        ax1.xaxis.set_major_formatter(myFmt)
    ax1.set_ylim(0, fs / 2)
    ax1.set_ylabel('frequency [Hz]')

    ax2 = fig.add_subplot(212)
    # note that the period has to be inverted both in the vector and the spectrum,
    # as pcolormesh wants to have a positive difference between samples
    ax2.pcolormesh(t_edge, p_edge[::-1], np.log10(spec[:0:-1]))
    ax2.set_ylim(0, 100 / fs)
    ax2.set_xlabel('t [s]')
    ax2.set_ylabel('period [s]')


def butter_highpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = signal.butter(order, normal_cutoff, btype='high', analog=False)
    return b, a


def butter_highpass_filter(data, cutoff, fs, order=5):
    b, a = butter_highpass(cutoff, fs, order=order)
    y = signal.filtfilt(b, a, data)
    return y


def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = signal.butter(order, normal_cutoff, btype='low', analog=False)
    return b, a


def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = signal.filtfilt(b, a, data)
    return y