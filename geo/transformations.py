from pyproj import Proj, transform

def WGS84toRD(longitude, latitude):

	# Define projections
	proj_RD = Proj(init='epsg:28992')
	proj_WGS84 = Proj(init='epsg:4326')
	
	# Transform coordinates
	return transform(proj_WGS84, proj_RD, longitude, latitude) 
	
