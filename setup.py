#!/usr/bin/env python

from distutils.core import setup

setup(name='Pytoolserik',
      version='1.0',
      description='Random set of helper functions for Erik',
      author='Erik Duisterwinkel',
      author_email='erik.duisterwinkel@anteagroup.com',
      url='',
      packages=[],
     )